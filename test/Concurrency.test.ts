import {Document, model, Model, Mongoose, Schema} from "mongoose";
import {PrepMongoose} from "./PrepMongoose";
import {installVersioningMiddlewareOnSchema, ISpeciesModel, SpeciesSchema} from "../src/Species";

let Species: Model<ISpeciesModel>;

async function prepMongoose(versioning: boolean = false) {
  if (versioning) {
    installVersioningMiddlewareOnSchema(SpeciesSchema);
  }
  const mongoose = new Mongoose();
  Species = mongoose.model<ISpeciesModel>("Species", SpeciesSchema);
  await PrepMongoose.prep(mongoose);
}

afterEach(async () => {
  await Species.db.dropCollection(Species.collection.name);
});


function incrementUpdatesCount(entity: ISpeciesModel) {
  entity.updatesCount = entity.updatesCount ? entity.updatesCount + 1 : 1;
}

async function user1ChangesDogInCat(entity: ISpeciesModel) {
  incrementUpdatesCount(entity);
  entity.species = "kitty";
  entity.cry = "meow";
  await entity.save();
  await showEntity(entity._id, "after saving cat");
}

async function user2UpdatesDogNameWithoutTouchingCrySinceItIsAlreadyOk(entity: ISpeciesModel) {
  incrementUpdatesCount(entity);
  if (entity.species != "dog") {
    entity.cry = "wouf wouf";
  }
  entity.species = "doggy";
  await entity.save();
  await showEntity(entity._id, "after saving dog");
}

async function createEntity(species: string, cry: string) {
  const entityToSave = new Species({species: species, cry: cry, updatesCount: 0});
  const entity = await entityToSave.save();
  const id = entity._id;
  console.log(`just saved entity id: ${id}`);
  await showEntity(id, "created entity");
  return id;
}

async function showEntity(id: string, message: string) {
  let entity = await Species.findById(id);
  console.info(`${message}: ${entity}`);
}


describe("Write concurrency without Versioning", () => {

  async function changeBothEntitiesAtTheSameTime(versioning?: boolean) {
    const entityId = await createEntity("dog", "wouf wouf");

    const entity1 = await Species.findById(entityId);
    const entity2 = await Species.findById(entityId);
    if (!entity1 || !entity2) {
      throw new Error(`findById failed, e1: ${entity1}, e2: ${entity2}`);
    }

    await user1ChangesDogInCat(entity1);
    await user2UpdatesDogNameWithoutTouchingCrySinceItIsAlreadyOk(entity2);

    let screwedUpEntity = await Species.findById(entityId);
    if (!screwedUpEntity) {
      throw new Error(`findById failed, entity: ${screwedUpEntity}`);
    }
    return screwedUpEntity;
  }

  test("On same entity, read at the same version, second write is allowed which leads to an invalid state", async () => {
    // given
    await prepMongoose();

    // when
    let screwedUpEntity = await changeBothEntitiesAtTheSameTime();

    // then
    expect(screwedUpEntity.species).toBe("doggy");
    assertABusinessRuleWasBrokenSinceTheSecondWriteWasAllowed(screwedUpEntity);
    assertAnUpdateWasLostSinceTheSecondWriteWasAllowed(screwedUpEntity);
  });

  test("second write throws an error since versioning/optimistic locking prevents us from corrupting the entity state ", async () => {
    // given
    const enableVersioning = true;
    await prepMongoose(enableVersioning);

    // when
    try {
      await changeBothEntitiesAtTheSameTime();
      throw new Error("a VersionError should have been raised before reaching this point");
    } catch (e) {
      expect(e.name).toBe("VersionError");
    }
  });

  function assertABusinessRuleWasBrokenSinceTheSecondWriteWasAllowed(entity: ISpeciesModel) {
    expect(entity.cry).toBe("meow");
  }

  function assertAnUpdateWasLostSinceTheSecondWriteWasAllowed(entity: ISpeciesModel) {
    expect(entity.updatesCount).toBe(1);
  }
});
